# CVS Client

Web-based CVS client. Similar to GitHub, but it was for CVS and it predates GitHub over 10 years.


CVS, concurrent versioning system, supercedes RCS (one of the earliest version control system on Unix),
and it is superceded by SVN (Subversion).


CVS was one of the most popular open-source VCS's at the time (ca 2000?),
and I was trying build a web-based client system.


Not sure how complete the system was at this point though...

