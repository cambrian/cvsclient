package com.cvsclient.proxy;

import javax.ejb.EJBObject;
import java.rmi.RemoteException;

public interface Proxy extends EJBObject {
 
   public double dollarToYen(double dollars) throws RemoteException;
   public double yenToEuro(double yen) throws RemoteException;
}

