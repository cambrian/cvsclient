package com.cvsclient.proxy;


import java.rmi.RemoteException; 
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

public class ProxyBean implements SessionBean {
 
   public double dollarToYen(double dollars) {

      return dollars * 121.6000;
   }

   public double yenToEuro(double yen) {

      return yen * 0.0077;
   }

   public ProxyBean() {}
   public void ejbCreate() {}
   public void ejbRemove() {}
   public void ejbActivate() {}
   public void ejbPassivate() {}
   public void setSessionContext(SessionContext sc) {}

} // ProxyBean

