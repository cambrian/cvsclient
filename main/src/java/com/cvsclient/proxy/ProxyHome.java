package com.cvsclient.proxy;


import java.io.Serializable;
import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface ProxyHome extends EJBHome {

    Proxy create() throws RemoteException, CreateException;
}

