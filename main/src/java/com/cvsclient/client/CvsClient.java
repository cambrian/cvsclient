package com.cvsclient.client;


import com.cvsclient.proxy.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

public class CvsClient {

   public static void main(String[] args) {
       try {
           Context initial = new InitialContext();
           Object objref = initial.lookup("java:comp/env/ejb/SimpleCvsClient");

           ProxyHome home = 
               (ProxyHome)PortableRemoteObject.narrow(objref, 
                                            ProxyHome.class);

           Proxy currencyProxy = home.create();
           double amount = currencyProxy.dollarToYen(100.00);
           System.out.println(String.valueOf(amount));
           amount = currencyProxy.yenToEuro(100.00);
           System.out.println(String.valueOf(amount));

           System.exit(0);

       } catch (Exception ex) {
           System.err.println("Caught an unexpected exception!");
           ex.printStackTrace();
       }
   } 
} 


