<%@ page import="com.cvsclient.proxy.*,javax.ejb.*, javax.naming.*, javax.rmi.PortableRemoteObject, java.rmi.RemoteException" %>
<%!
   private Proxy proxy = null;

   public void jspInit() { 
      try {
         InitialContext ic = new InitialContext();
         Object objRef = ic.lookup("java:comp/env/ejb/TheProxy");
         ProxyHome home = (ProxyHome)PortableRemoteObject.narrow(objRef, ProxyHome.class);
         proxy = home.create();
      } catch (RemoteException ex) {
            System.out.println("Couldn't create proxy bean."+ ex.getMessage());
      } catch (CreateException ex) {
            System.out.println("Couldn't create proxy bean."+ ex.getMessage());
      } catch (NamingException ex) {
            System.out.println("Unable to lookup home: "+ "TheProxy "+ ex.getMessage());
      } 
   }

   public void jspDestroy() {    
         proxy = null;
   }
%>
<html>
<head>
    <title>Proxy</title>
</head>

<body bgcolor="white">
<h1><b><center>Proxy</center></b></h1>
<hr>
<p>Enter an amount to convert:</p>
<form method="get">
<input type="text" name="amount" size="25">
<br>
<p>
<input type="submit" value="Submit">
<input type="reset" value="Reset">
</form>

<%
    String amount = request.getParameter("amount");
    if ( amount != null && amount.length() > 0 ) {
       Double d = new Double (amount);
%>
   <p>
   <%= amount %> dollars are  <%= proxy.dollarToYen(d.doubleValue()) %>  Yen.
   <p>
   <%= amount %> Yen are <%= proxy.yenToEuro(d.doubleValue()) %>  Euro.
<%
    }
%>

</body>
</html>
