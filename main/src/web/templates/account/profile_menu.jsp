<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Profile Menu</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/cvsclient.css" type="text/css">
</HEAD>
<BODY class="sidemenu" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">

<TABLE class="menu-border" BORDER=0 CELLSPACING=0 CELLPADDING=0
WIDTH=100%>

<TR class="menu-blank-row" height="5">
  <TD width="100" height="5">
    <IMG border="0" src="images/blank.gif" width="100" height="5">
  </TD>
</TR>

<TR><TD><TABLE BORDER=0 CELLSPACING=1 CELLPADDING=0 WIDTH=100%>

<TR class="menu-odd-row" VALIGN=TOP HEIGHT="30">
  <TD CLASS="menu-header" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH="30">
    <TABLE BORDER=0 HEIGHT=100% WIDTH=100%>
    <TR><TD CLASS="menu-header-inside" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH=100%>
    <A HREF="#" TARGET="SIDEMENU">
    <IMG src="images/plus.gif" border="0" alt=""></A>
    </TD></TR>
    </TABLE>
  </TD>

  <TD CLASS="menu-body" VALIGN=MIDDLE HEIGHT="30" WIDTH="120">
    <A href="profile_front.jsp" target="BODY"
    onMouseOver="self.status='Front Page'; return true;"
    onMouseOut="self.status=''; return true;">Front Page</A>
  </TD>
</TR>

<TR class="menu-odd-row" VALIGN=TOP HEIGHT="30">
  <TD CLASS="menu-header" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH="30">
    <TABLE BORDER=0 HEIGHT=100% WIDTH=100%>
    <TR><TD CLASS="menu-header-inside" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH=100%>
    <A HREF="#" TARGET="SIDEMENU">
    <IMG src="images/plus.gif" border="0" alt=""></A>
    </TD>
    </TR>
    </TABLE>
  </TD>

  <TD CLASS="menu-body" VALIGN=MIDDLE HEIGHT="30" WIDTH="120">
    <A href="profile_myrepo.jsp" target="BODY"
    onMouseOver="self.status='My Repositories'; return true;"
    onMouseOut="self.status=''; return true;">My Repositories</A>
  </TD>
</TR>

<TR class="menu-odd-row" VALIGN=TOP HEIGHT="30">
  <TD CLASS="menu-header" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH="30">
    <TABLE BORDER=0 HEIGHT=100% WIDTH=100%>
    <TR><TD CLASS="menu-header-inside" ALIGN=CENTER VALIGN=MIDDLE HEIGHT="30" WIDTH=100%>
    <A HREF="#" TARGET="SIDEMENU">
    <IMG src="images/plus.gif" border="0" alt=""></A>
    </TD>
    </TR>
    </TABLE>
  </TD>

  <TD CLASS="menu-body" VALIGN=MIDDLE HEIGHT="30" WIDTH="120">
    <A href="profile_setting.jsp" target="BODY"
    onMouseOver="self.status='CVS Client Setting'; return true;"
    onMouseOut="self.status=''; return true;">Client Setting</A>
  </TD>
</TR>

</TABLE></TD></TR>
</TABLE>

</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->
