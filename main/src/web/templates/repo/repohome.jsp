<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository | Repository Home</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/repo.css" type="text/css">
</HEAD>
<BODY class="body">

<!-- Begin: Header -->
<DIV class="date">
<SCRIPT type="text/javascript" language="JavaScript" src="<%=request.getContextPath()%>/scripts/currenttime.js"></SCRIPT>
</DIV>
<DIV align="center">
<TABLE class="footer" border=0 cellPadding=1 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD class="horzbar" width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="2"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="10"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
<!-- End: Header -->



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part: Left Column -->
    <TD vAlign="top" width="60%">

      <TABLE border="0" cellPadding="5" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="fancybox">
            <TR class="header">
              <TD>What's New?</TD>
            </TR>
            <TR>
              <TD>

              ....

              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD>Home Page</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>     
            <TR class="header">
              <TD>Account</TD>
            </TR>
            <TR>
              <TD>
                Login
              </TD>
            </TR>
            <TR>
              <TD>
                Sign-up
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD>CVS Repository</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>     
            <TR class="header">
              <TD>Work Bench</TD>
            </TR>
            <TR>
              <TD>
                ...
              </TD>
            </TR>
          </TABLE>
      
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part: Left Column -->
    
    <!-- Begin: Main Part: Right Column -->
    <TD vAlign="top" width="40%">

      <TABLE border="0" cellPadding="5" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="fancybox">
            <TR class="header">
              <TD noWrap>Services</TD>
            </TR>
            <TR>
              <TD>
                <DL>
                  <DT>FTP Directories</DT>
                  <DD>...</DD>
                  <DT>Bug Tracker</DT>
                  <DD>...</DD>
                  <DT>Community</DT>
                  <DD>...</DD>
                </DL>
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD noWrap>Information</TD>
            </TR>
            <TR>
              <TD>
                About<BR>
                Terms and Conditions<BR>
                Contact Information
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        </TBODY>
      </TABLE>

    </TD>
    <!-- End: Main Part: Right Column -->

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<!-- Begin: Footer -->
<DIV align="center">
<TABLE class="footer" border=0 cellPadding=1 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="10"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD class="horzbar" width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="2"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
<DIV class="copyright">
<SCRIPT type="text/javascript" language="JavaScript" src="<%=request.getContextPath()%>/scripts/footer.js"></SCRIPT>
</DIV>
<!-- End: Footer -->


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

