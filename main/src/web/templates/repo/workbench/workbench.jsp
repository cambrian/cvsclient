<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="robot" CONTENT="all">
<META name="summary" content="CVS Client Home Page">
<META name="keywords" content="CVS, Client">
<TITLE>CVS Client | Repository | Work Bench</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/workbench.css" type="text/css">
</HEAD>

<FRAMESET rows="20,*" frameborder="0" border="0" framespacing="0" framepadding="0">
    <FRAME src="clientmenu.jsp" name="TOPMENU"
           frameborder="0" marginheight="0" marginwidth="0"
           scrolling="no" noresize/>
    <FRAME src="manager.jsp" name="BODYFRAME"
           frameborder="0" marginheight="0" marginwidth="0"
           scrolling="no"/>
</FRAMESET>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->

