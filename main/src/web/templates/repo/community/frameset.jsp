<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository | Community</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/repo.css" type="text/css">
</HEAD>

<FRAMESET rows="*,1" frameborder="0" border="0" framespacing="0" framepadding="0">
    <FRAMESET cols="150,*" frameborder="0" border="0" framespacing="0" framepadding="0">
        <FRAME src="leftmenu.jsp" name="SIDEMENU" 
               marginheight="0" marginwidth="0"
               scrolling="no" noresize/>
        <FRAME src="community.jsp" name="BODY"
               marginheight="0" marginwidth="0"
               scrolling="auto"/>
    </FRAMESET>
    <FRAME src="blank.jsp" name="FOOT" 
           marginheight="0" marginwidth="0"
           scrolling="no" noresize/>
</FRAMESET>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->

