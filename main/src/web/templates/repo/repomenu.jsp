<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/repo.css" type="text/css">
</HEAD>


<BODY class="topmenu" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">

<TABLE margin="0" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>
    <TD height="20" vAlign="center" noWrap>
      &nbsp;&nbsp;<A href="frameset.jsp"
      onMouseOver="self.status='Repository Home'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Repository&nbsp;Home</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="cvsmanager/frameset.jsp"
      onMouseOver="self.status='CVS Manager'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">CVS&nbsp;Manager</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="bugtracker/frameset.jsp"
      onMouseOver="self.status='Bug Tracker'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Bug&nbsp;Tracker</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="community/frameset.jsp"
      onMouseOver="self.status='Community'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Community</A>&nbsp;&nbsp;|
    </TD>
  </TR>
  </TBODY>
</TABLE>
      
</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->

