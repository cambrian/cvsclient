<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository | Home | Front Page</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/repo.css" type="text/css">
</HEAD>
<BODY class="body">

<!-- Begin: Header -->
<DIV align="center">

<TABLE border=0 cellPadding=5 cellSpacing=1 width="100%">
  <TBODY>
  <TR nowrap>
    <TD class="titlebox">
      <TABLE border=0 cellPadding=0 cellSpacing=0>
        <TBODY>
        <TR align=middle>
          <TD width="2%"><A href="#"><IMG 
            alt="CVSClient" border=0 height="32" width="100"
            src="#"></A></TD>
          <TD width="1%"><IMG alt="" border=0 height="32" width="10"
            src="<%=request.getContextPath()%>/images/blank.gif"></TD>
          <TD class="title" align=left width="95%">CVS Client Home</TD>
          <TD width="1%"><IMG alt="" border=0 height="32" width="10"
            src="<%=request.getContextPath()%>/images/blank.gif"></TD>
          <TD width="1%"><A href="#"><IMG alt="Help" border=0 height="32" width="32"
            src="<%=request.getContextPath()%>/images/help.gif"></A></TD>
        </TR>
        </TBODY>
      </TABLE>
  </TR>
  <TR>
    <TD colspan="5"><IMG alt="" border=0 height="5" width="200"
     src="images/blank.gif"></TD>
  </TR>
</TBODY>
</TABLE>

</DIV>
<!-- End: Header -->



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part -->
    <TD vAlign="top" width="*">


<H3>Research Interests</H3>
<P>
I'm mainly interested in understanding higher brain functions
and its application to implementing intelligent softwares.
You can find more detailed explanations on
my current research areas from
<A href="research.html" title="Research Interests">this link</A>.
</P>

    </TD>
    <!-- End: Main Part -->

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<!-- Begin: Footer -->
<DIV align="center">
<TABLE class="footer" border=0 cellPadding=1 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="10"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD class="horzbar" width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="2"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
<DIV class="copyright">
<SCRIPT type="text/javascript" language="JavaScript"
src="footer.js">
</SCRIPT>
</DIV>
<!-- End: Footer -->


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

