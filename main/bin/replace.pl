#!/usr/bin/perl

# Multiple files can be given as input
for($i=0;$i<@ARGV;$i++) {
    $tmpfile = $ARGV[$i] . "_tmp";
    rename($ARGV[$i],$tmpfile);
    $newfile = $ARGV[$i];

    open(HLFILE, $tmpfile) || die "Sorry, I couldn't open $tmpfile\n";
    open(HFILE, ">".$newfile) || die "Sorry, I couldn't open $newfile\n";
    while(<HLFILE>) {
        s/\"\/styles/\"<%=request.getContextPath()%>\/styles/g;
        s/\"\/images/\"<%=request.getContextPath()%>\/images/g;
        s/\"\/icons/\"<%=request.getContextPath()%>\/icons/g;
        s/\"\/scripts/\"<%=request.getContextPath()%>\/scripts/g;
        s/href=\"\//href=\"<%=request.getContextPath()%>\/templates\//g;

        print HFILE $_;
    }
    close(HFILE);
    close(HLFILE);
    unlink($tmpfile);
}
