echo ************************************************************ 
echo Resetting Environment Variables
echo ************************************************************ 

set JDKDIR=
set JREDIR=
set JAVACOMPILE=
set WORKDIR=
set SOURCE_DIR=
set SOURCE_FILES=
set OUTPUT_FILES=
set IDLGEN_DIR=
set IDLGEN_OUTPUT_FILES=
set IDLCLASSPATH=
set IDLGEN_JAR=
set LIB_JARS=
set IONA_HOME=
set IONA_IDL_DIR=
set IONA_JAR_HOME=
set IONA_CONFIG_DIR=
set IONA_CONTRIB_CLASSES=
set DB_DRIVER=


set IT_PRODUCT_DIR=%OLD_IT_PRODUCT_DIR%
set CLASSPATH=%OLD_CLASSPATH%

set OLD_IT_PRODUCT_DIR=
set OLD_CLASSPATH=

