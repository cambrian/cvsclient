echo ************************************************************ 
echo Setting Environment Variables
echo ************************************************************ 

set OLD_IT_PRODUCT_DIR=%IT_PRODUCT_DIR%
set OLD_CLASSPATH=%CLASSPATH%


if "%JAVA_HOME%" == "" set JAVA_HOME=H:\jdk1.3.1
set JDKDIR=%JAVA_HOME%
set JREDIR=%JAVA_HOME%\jre
set JAVACOMPILE=%JDKDIR%\bin\javac


if "%CVSCLIENT_ROOT%" == "" set CVSCLIENT_ROOT=F:\cvsclient\main
if "%DEV_ROOT%" == "" set DEV_ROOT=%CVSCLIENT_ROOT%\build
set SOURCE_DIR=%CVSCLIENT_ROOT%\src\java
set SOURCE_FILES=%SOURCE_DIR%;%DEV_ROOT%
set WORKDIR=%DEV_ROOT%
set OUTPUT_FILES=%WORKDIR%\java
set IDLGEN_DIR=%WORKDIR%\idlgen
set IDLGEN_OUTPUT_FILES=%WORKDIR%\idlgen\classes
set LIB_JARS=CVSCLIENT_ROOT\lib\comutil.jar
set IDLGEN_JAR=%WORKDIR%\lib\idlgen.jar

set DB_DRIVER=
REM set DB_DRIVER=%workdir%\lib\db2java.zip

if "%IT_PRODUCT_DIR%" == "" set IT_PRODUCT_DIR=H:\iona
set IONA_HOME=%IT_PRODUCT_DIR%
set IONA_IDL_DIR=%IONA_HOME%\orbix_art\1.2\idl
set IONA_JAR_HOME=%IONA_HOME%\orbix_art\1.2\classes
set IONA_CONFIG_DIR=%IONA_HOME%\orbix_art\1.2;%IONA_HOME%\orbix_art\1.2\localhost;%IONA_HOME%\etc
set IONA_CONTRIB_CLASSES=%IONA_HOME%\orbix_art\1.2\contrib\classes\jdeps.jar


set IDLCLASSPATH=%IDLGEN_OUTPUT_FILES%;%DB_DRIVER%;%LIB_JARS%;%JDKDIR%\lib\tools.jar;%JDKDIR%\lib\dt.jar;%JREDIR%\lib\jaws.jar;%JREDIR%\lib\rt.jar;%JREDIR%\lib\i18n.jar;%IONA_JAR_HOME%\orbix2000.jar;%IONA_JAR_HOME%\omg.jar;%WORKDIR%;%IONA_CONTRIB_CLASSES%;%IONA_CONFIG_DIR%

set CLASSPATH=%IDLGEN_JAR%;%OUTPUT_FILES%;%DB_DRIVER%;%LIB_JARS%;%JDKDIR%\lib\tools.jar;%JDKDIR%\lib\dt.jar;%JREDIR%\lib\jaws.jar;%JREDIR%\lib\rt.jar;%JREDIR%\lib\i18n.jar;%IONA_JAR_HOME%\orbix2000.jar;%IONA_JAR_HOME%\omg.jar;%WORKDIR%;%IONA_CONTRIB_CLASSES%;%IONA_CONFIG_DIR%


