@echo off
REM ************************************************************ 
REM Script to call replace.pl to multiple files.
REM ************************************************************ 

if "%CVSCLIENT_ROOT%" == "" set CVSCLIENT_ROOT=F:\cvsclient\main
echo [[[ Calling replace.pl ...
for %%l IN (%CVSCLIENT_ROOT%\src\web\templates\*.jsp) DO call perl %CVSCLIENT_ROOT%\bin\replace.pl %%l

@echo on
