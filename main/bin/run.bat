@echo off
REM ************************************************************ 
REM Script to run test servers. 
REM Prerequisite:
REM    setenv and unsetenv scripts.
REM ************************************************************ 


REM Setting environment variables.
call setenv


if "%1" == "server" goto SERVER
if "%1" == "client" goto CLIENT
if "%1" == "webclient" goto WEBCLIENT
goto SERVER


:SERVER
if "%2" == "start" goto START
if "%2" == "stop" goto STOP
goto START

:START
echo [[[ Starting the server...                               ]]] 
REM %JREDIR%\bin\java -classpath %CLASSPATH%  com.cvsclient.server.proto %1 %2 %3 %4 %5 %6 %7 %8 %9
start j2ee -verbose
goto CLEANUP

:STOP
echo [[[ Stopping the server...                               ]]] 
j2ee -stop
goto CLEANUP


:CLIENT
echo [[[ Starting the client...                               ]]] 
REM %JREDIR%\bin\java -classpath %CLASSPATH%  com.cvsclinet.client.proto %1 %2 %3 %4 %5 %6 %7 %8 %9
set APPCPATH=%SOURCE_DIR%\com\cvsclient\cvsclientClient.jar
start runclient -client %SOURCE_DIR%\com\cvsclient\cvsclient.ear -name CvsClient
set APPCPATH=
goto CLEANUP


:WEBCLIENT
echo [[[ Starting the webclient...                               ]]] 
start http://localhost:8000/cvsclient
goto CLEANUP


:CLEANUP
REM Cleaning up environment variables.
call unsetenv


:END
@echo on
