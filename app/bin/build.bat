@echo off

REM ----------------------------------------------------------
REM CVSClient Main Build Script
REM This script is tested under Windows NT & Win98
REM ----------------------------------------------------------

setlocal

REM -- root directory for the project
set _PROJECTDIR=%PROJECTDIR%
set PROJECTDIR=F:\cvsclient\app

REM -- Directory containing jars required for runtime
set _LIBDIR=%LIBDIR%
set LIBDIR=%PROJECTDIR%\lib

REM -- Bin directory
set _BINDIR=%BINDIR%
set BINDIR=%PROJECTDIR%\bin

REM -- Directory containing jars required for compilation
set _BUILDLIBDIR=%BUILDLIBDIR%
set BUILDLIBDIR=%BINDIR%\lib

REM -- Name of the build file to use
set _BUILDFILE=%BUILDFILE%
set BUILDFILE=%BINDIR%\build.xml

REM --------------------------------------------
REM No need to edit anything past here
REM --------------------------------------------

:init
set _CLASSPATH=%CLASSPATH%
set LOCALPATH=.

:buildpath
REM for %%l IN (%LIBDIR%\*.jar) DO call %BINDIR%\addlcp %%l
for %%l IN (%BUILDLIBDIR%\*.jar) DO call %BINDIR%\addlcp %%l

:testjavahome
set _JAVA_HOME=%JAVA_HOME%
set JAVA_HOME=H:\jdk1.4
REM if "%JAVA_HOME%" == "" goto setjavahome
goto build

:setjavahome
if not "%OS%" == "Windows_NT" goto javahomeerror

:setjavahoment
for %%j IN (java.exe) DO set JAVABIN=%%~dp$PATH:j
if "%JAVABIN%" == "" goto javahomeerror
for %%j IN (%JAVABIN%..\) DO set JAVA_HOME=%%~dpj

:build
if exist %JAVA_HOME%\lib\tools.jar set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar

%JAVA_HOME%\bin\java.exe -classpath "%LOCALPATH%;%CLASSPATH%" org.apache.tools.ant.Main -buildfile %BUILDFILE% %1

goto end

:javahomeerror
echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.

:end

set JAVA_HOME=%_JAVA_HOME%
set CLASSPATH=%_CLASSPATH%
set PROJECTDIR=%_PROJECTDIR%
set LIBDIR=%_LIBDIR%
set BINDIR=%_BINDIR%
set BUILDLIBDIR=%_BUILDLIBDIR%
set BUILDFILE=%_BUILDFILE%

endlocal
