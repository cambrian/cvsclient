@echo off
rem ---------------------------------------------------------------------------
rem startup.bat - Start Script for the CATALINA Server
rem
rem $Id: startup.bat,v 1.1 2001/08/27 01:24:55 hyoon Exp $
rem ---------------------------------------------------------------------------

REM set _JAVA_HOME=%JAVA_HOME%
REM set JAVA_HOME=H:\jdk1.4

set _CATALINA_HOME=%CATALINA_HOME%
if not "%CATALINA_HOME%" == "" goto gotCatalinaHome
set CATALINA_HOME=H:\jakarta\tomcat-4.0-b7
if exist "%CATALINA_HOME%\bin\catalina.bat" goto gotCatalinaHome
set CATALINA_HOME=..
if exist "%CATALINA_HOME%\bin\catalina.bat" goto gotCatalinaHome
echo Unable to determine the value of CATALINA_HOME
goto cleanup
:gotCatalinaHome
REM set CATALINA_OPTS=-Djava.security.policy==..\conf\cvsclient_java2.policy -Djava.security.auth.policy==..\conf\cvsclient_jaas.policy -Djava.security.auth.login.config==..\conf\cvsclient_jaas.config
set CATALINA_OPTS=-Djava.security.auth.policy==F:\cvsclient\app\conf\cvsclient_jaas.policy -Djava.security.auth.login.config==F:\cvsclient\app\conf\cvsclient_jaas.config
"%CATALINA_HOME%\bin\catalina" start -security %1 %2 %3 %4 %5 %6 %7 %8 %9
:cleanup
set CATALINA_HOME=%_CATALINA_HOME%
set _CATALINA_HOME=

REM set JAVA_HOME=%_JAVA_HOME%

