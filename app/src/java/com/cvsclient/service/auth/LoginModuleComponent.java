/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LoginModuleComponent.java,v 1.3 2001/08/30 05:44:01 hyoon Exp $
 */ 
package com.cvsclient.service.auth;

import  com.cvsclient.common.auth.*;
import  java.util.*;
import  org.apache.avalon.framework.*;
import  org.apache.avalon.framework.activity.*;
import  org.apache.avalon.framework.component.*;
import  org.apache.avalon.framework.configuration.*;
import  org.apache.avalon.framework.context.*;
import  org.apache.avalon.framework.logger.*;
import  org.apache.avalon.framework.parameters.*;
import  org.apache.avalon.framework.thread.*;
import  javax.security.auth.Subject;
import  javax.security.auth.login.LoginException;
import  javax.security.auth.callback.CallbackHandler;


/**
 * LoginModuleComponent
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class LoginModuleComponent
    extends LoginModuleBase
    implements Component, Configurable, Composable, Initializable, Disposable, ThreadSafe
{
    private boolean initialized = false;
    private boolean disposed = false;
    private ComponentManager manager = null;
    private String dbResource = null;

    /**
     * Constructor.  All Components need a public no argument constructor
     * to be a legal Component.
     */
    public LoginModuleComponent() {}

    /**
     * Configuration.  Notice that I check to see if the Component has
     * already been configured?  This is done to enforce the policy of
     * only calling Configure once.
     */
    public final void configure(Configuration conf)
        throws ConfigurationException
    {
        if (initialized || disposed) {
            throw new IllegalStateException ("Illegal call");
        }

        if (null == this.dbResource) {
		    this.dbResource = conf.getChild("dbpool").getValue();
            getLogger().debug("Using database pool: " + this.dbResource);
        }
    }

    /**
     * Composition.  Notice that I check to see if the Component has
     * already been initialized or disposed?  This is done to enforce
     * the policy of proper lifecycle management.
     */
    public final void compose(ComponentManager cmanager)
        throws ComponentException
    {
        if (initialized || disposed) {
            throw new IllegalStateException ("Illegal call");
        }

        if (null == this.manager) {
            this.manager = cmanager;
        }
    }

    public final void initialize()
        throws Exception
    {
        if (null == this.manager) throw new IllegalStateException("Not Composed");
        if (null == this.dbResource)
            throw new IllegalStateException("Not Configured");

        if (disposed) throw new IllegalStateException("Already disposed");

        this.initialized = true;
    }

    public final void dispose() {
        this.disposed = true;
    }
    

    
    ///////////////////////////////////////////////////////////////////////
    // Methods defined in LoginModule
    ///////////////////////////////////////////////////////////////////////

    /**
     * Initialize this <code>LoginModule</code>.
     *
     * <p>
     *
     * @param subject the <code>Subject</code> to be authenticated. <p>
     *
     * @param callbackHandler a <code>CallbackHandler</code> for communicating
     *			with the end user (prompting for usernames and
     *			passwords, for example). <p>
     *
     * @param sharedState shared <code>LoginModule</code> state. <p>
     *
     * @param options options specified in the login
     *			<code>Configuration</code> for this particular
     *			<code>LoginModule</code>.
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler,
                           Map sharedState, Map options)
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        super.initialize(subject, callbackHandler, sharedState, options);
    }

    /**
     * Authenticate the user by prompting for a username and password.
     *
     * <p>
     *
     * @return true in all cases since this <code>LoginModule</code>
     *		should not be ignored.
     *
     * @exception FailedLoginException if the authentication fails. <p>
     *
     * @exception LoginException if this <code>LoginModule</code>
     *		is unable to perform the authentication.
     */
    public boolean login() throws LoginException
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        return super.login();
    }


    /**
     * <p> This method is called if the LoginContext's
     * overall authentication succeeded
     * (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules
     * succeeded).
     *
     * <p> If this LoginModule's own authentication attempt
     * succeeded (checked by retrieving the private state saved by the
     * <code>login</code> method), then this method associates a
     * <code>DefaultPrincipal</code>
     * with the <code>Subject</code> located in the
     * <code>LoginModule</code>.  If this LoginModule's own
     * authentication attempted failed, then this method removes
     * any state that was originally saved.
     *
     * <p>
     *
     * @exception LoginException if the commit fails.
     *
     * @return true if this LoginModule's own login and commit
     *		attempts succeeded, or false otherwise.
     */
    public boolean commit() throws LoginException
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        return super.commit();
    }
    
        
    /**
     * <p> This method is called if the LoginContext's
     * overall authentication failed.
     * (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules
     * did not succeed).
     *
     * <p> If this LoginModule's own authentication attempt
     * succeeded (checked by retrieving the private state saved by the
     * <code>login</code> and <code>commit</code> methods),
     * then this method cleans up any state that was originally saved.
     *
     * <p>
     *
     * @exception LoginException if the abort fails.
     *
     * @return false if this LoginModule's own login and/or commit attempts
     *		failed, and true otherwise.
     */
    public boolean abort() throws LoginException
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        return super.abort();
    }
    
    /**
     * Logout the user.
     *
     * <p> This method removes the <code>DefaultPrincipal</code>
     * that was added by the <code>commit</code> method.
     *
     * <p>
     *
     * @exception LoginException if the logout fails.
     *
     * @return true in all cases since this <code>LoginModule</code>
     *          should not be ignored.
     */
    public boolean logout() throws LoginException
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        return super.logout();
    }
    
}




