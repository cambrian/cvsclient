/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: VersionControlBase.java,v 1.1 2001/08/30 05:44:01 hyoon Exp $
 */ 
package com.cvsclient.service.vc;

import  com.cvsclient.*;
import  java.util.*;
import  java.io.File;
import  java.io.IOException;
import  org.apache.avalon.framework.*;
import  org.apache.avalon.framework.logger.*;


/**
 * VersionControlBase
 *
 * @version $Revision: 1.1 $
 * @author Hyoungsoo Yoon
 */
public class VersionControlBase
    extends AbstractLoggable
    implements VersionControl
{

    public void login(String username, String password)
    {
    }
    
    public void logout()
    {
    }
    
    public void invoke(String command, String[] args)
    {
    }

}

