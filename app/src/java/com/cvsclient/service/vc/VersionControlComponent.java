/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: VersionControlComponent.java,v 1.5 2001/08/30 05:44:01 hyoon Exp $
 */ 
package com.cvsclient.service.vc;

import  com.cvsclient.VersionControl;
import  org.apache.avalon.framework.*;
import  org.apache.avalon.framework.activity.*;
import  org.apache.avalon.framework.component.*;
import  org.apache.avalon.framework.configuration.*;
import  org.apache.avalon.framework.context.*;
import  org.apache.avalon.framework.logger.*;
import  org.apache.avalon.framework.parameters.*;
import  org.apache.avalon.framework.thread.*;


/**
 * VersionControlComponent
 *
 * @version $Revision: 1.5 $
 * @author Hyoungsoo Yoon
 */
public class VersionControlComponent
    extends VersionControlBase
    implements VersionControl, Component, Configurable, Composable, Initializable, Disposable, ThreadSafe
{
    private boolean initialized = false;
    private boolean disposed = false;
    private ComponentManager manager = null;
    private String dbResource = null;

    /**
     * Constructor.  All Components need a public no argument constructor
     * to be a legal Component.
     */
    public VersionControlComponent() {}

    /**
     * Configuration.  Notice that I check to see if the Component has
     * already been configured?  This is done to enforce the policy of
     * only calling Configure once.
     */
    public final void configure(Configuration conf)
        throws ConfigurationException
    {
        if (initialized || disposed) {
            throw new IllegalStateException ("Illegal call");
        }

        if (null == this.dbResource) {
		    this.dbResource = conf.getChild("dbpool").getValue();
            getLogger().debug("Using database pool: " + this.dbResource);
        }
    }

    /**
     * Composition.  Notice that I check to see if the Component has
     * already been initialized or disposed?  This is done to enforce
     * the policy of proper lifecycle management.
     */
    public final void compose(ComponentManager cmanager)
        throws ComponentException
    {
        if (initialized || disposed) {
            throw new IllegalStateException ("Illegal call");
        }

        if (null == this.manager) {
            this.manager = cmanager;
        }
    }

    public final void initialize()
        throws Exception
    {
        if (null == this.manager) throw new IllegalStateException("Not Composed");
        if (null == this.dbResource)
            throw new IllegalStateException("Not Configured");

        if (disposed) throw new IllegalStateException("Already disposed");

        this.initialized = true;
    }

    public final void dispose() {
        this.disposed = true;
    }



    ///////////////////////////////////////////////////////////////////////
    // Methods defined in VersionControl
    ///////////////////////////////////////////////////////////////////////

    public void login(String username, String password)
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        super.login(username, password);
    }
    
    public void logout()
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        super.logout();
    }

    public void invoke(String command, String[] args)
    {
        if (!initialized || disposed) {
            throw new IllegalStateException("Illegal call");
        }

        super.invoke(command, args);
    }
    
}




