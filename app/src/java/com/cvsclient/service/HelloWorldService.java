/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: HelloWorldService.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.service;

import  com.cvsclient.*;
import  java.util.*;
import  java.io.File;
import  java.io.IOException;
import  org.apache.avalon.framework.*;
import  org.apache.avalon.framework.logger.*;
import  org.apache.avalon.phoenix.Service;


/**
 * HelloWorldService
 *
 * @version $Revision: 1.1 $
 * @author Hyoungsoo Yoon
 */
public interface HelloWorldService
    extends Service
{
    String ROLE = HelloWorldService.class.toString();

    void setGreeting( String greeting );
}
