/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoDirectoryBase.java,v 1.2 2002/01/09 06:56:29 hyoon Exp $
 */ 
package com.cvsclient.service.repo;

import  com.cvsclient.*;
import  java.util.*;
import  java.io.File;
import  java.io.IOException;
import  org.apache.avalon.framework.*;
import  org.apache.avalon.framework.logger.*;


/**
 * RepoDirectoryBase
 *
 * @version $Revision: 1.2 $
 * @author Hyoungsoo Yoon
 */
public class RepoDirectoryBase
    extends AbstractLoggable
    implements RepoDirectory
{


    ///////////////////////////////////////////////////////////////////////
    // Methods defined in RepoDirectory
    ///////////////////////////////////////////////////////////////////////

    public long[]   listRepositories()
    {
        return null;
    }
    
    public void     addRepository(long id)
    {
    }
    
    public void     removeRepository(long id)
    {
    }

}

