/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SampleAction.java,v 1.1 2001/08/27 01:24:56 hyoon Exp $
 */ 
package com.cvsclient.beans.auth;

import  com.cvsclient.common.auth.*;
import  com.cvsclient.proxy.auth.*;
import  java.io.File;
import  java.security.PrivilegedAction;

/**
 * <p> This is a Sample PrivilegedAction implementation, designed to be
 * used with the Sample application.
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class SampleAction implements PrivilegedAction
{
    /**
     * <p> This Sample PrivilegedAction performs the following operations:
     * <ul>
     * <li> Access the System property, <i>java.home</i>
     * <li> Access the System property, <i>user.home</i>
     * <li> Access the file, <i>foo.txt</i>
     * </ul>
     *
     * @return <code>null</code> in all cases.
     *
     * @exception SecurityException if the caller does not have permission
     *		to perform the operations listed above.
     */
    public Object run()
    {
        System.out.println("\nYour java.home property: "
                           +System.getProperty("java.home"));

        System.out.println("\nYour user.home property: "
                           +System.getProperty("user.home"));

        File f = new File("foo.txt");
        System.out.print("\nfoo.txt does ");
        if (!f.exists())
            System.out.print("not ");
        System.out.println("exist in the current working directory.");
        return null;
    }
}

