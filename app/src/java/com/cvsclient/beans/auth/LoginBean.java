/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LoginBean.java,v 1.1 2001/08/27 01:24:56 hyoon Exp $
 */ 
package com.cvsclient.beans.auth;

import  com.cvsclient.common.auth.*;
import  com.cvsclient.proxy.auth.*;
import  java.io.*;
import  java.util.*;
import  java.security.Principal;
import  javax.security.auth.*;
import  javax.security.auth.callback.*;
import  javax.security.auth.login.*;
import  javax.security.auth.spi.*;
import  com.sun.security.auth.*;

/**
 * <p> This LoginBean application attempts to authenticate a user
 * and executes a SampleAction as that user.
 *
 * <p> If the user successfully authenticates itself,
 * the username and number of Credentials is displayed.
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class LoginBean
{
    private String m_username = "";
    private String m_password = "";
    
    public LoginBean()
    {
    }

    public String getUsername()
    {
        return m_username;
    }
    public void setUsername(String username)
    {
        m_username = username;
    }

    public String getPassword()
    {
        return m_password;
    }
    public void setPassword(String password)
    {
        m_password = password;
    }

    
    /**
     * Attempt to authenticate the user.
     *
     * <p>
     * 
     * @param args input arguments for this application.  These are ignored.
     */
    public boolean validate()
    {
        // use the configured LoginModules for the "CVSClient" entry
        LoginContext lc = null;
        try {
            lc = new LoginContext("CVSClient", new JspCallbackHandler(m_username, m_password));
        } catch (LoginException le) {
            le.printStackTrace();
            return false;
        } 

        // the user has 3 attempts to authenticate successfully
        int i;
        for (i = 0; i < 3; i++) {
            try {

                // attempt authentication
                lc.login();

                // if we return with no exception, authentication succeeded
                break;

            } catch (AccountExpiredException aee) {
                System.out.println("Your account has expired.  " +
                                   "Please notify your administrator.");
                return false;
            } catch (CredentialExpiredException cee) {
                System.out.println("Your credentials have expired.");
                return false;
            } catch (FailedLoginException fle) {
                System.out.println("Authentication Failed");
                try {
                    Thread.currentThread().sleep(3000);
                } catch (Exception e) {
                    // ignore
                }
            } catch (Exception e) {
                System.out.println("Unexpected Exception - unable to continue");
                e.printStackTrace();
                return false;
            }
        }

        // did they fail three times?
        if (i == 3) {
            System.out.println("Sorry");
            return false;
        } else {

            // let's see what Principals we have
            Iterator principalIterator = lc.getSubject().getPrincipals().iterator();
            System.out.println("Authenticated user has the following Principals:");
            while (principalIterator.hasNext()) {
                Principal p = (Principal)principalIterator.next();
                System.out.println("\t" + p.toString());
            }

            System.out.println("User has " +
                               lc.getSubject().getPublicCredentials().size() +
                               " Public Credential(s)");

            // now try to execute the SampleAction as the authenticated Subject
            //Subject.doAs(lc.getSubject(), new SampleAction());

        }
        
        return true;
    }
}

