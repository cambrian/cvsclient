/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: HelloBean.java,v 1.1 2001/08/25 07:01:53 hyoon Exp $
 */ 
package com.cvsclient.beans.common;

import  com.cvsclient.*;
import  java.util.*;

/**
 * The HelloBean 
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class HelloBean
{
    private String m_message;

    public HelloBean()
    {
        m_message = "Hello World!";
    }

    public String getMessage()
    {
        return m_message;
    }

    public void setMessage(String message)
    {
        m_message = message;
    }
}

