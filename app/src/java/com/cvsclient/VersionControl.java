/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: VersionControl.java,v 1.3 2001/08/30 04:22:49 hyoon Exp $
 */ 
package com.cvsclient;

/**
 * VersionControl
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public interface VersionControl
{
    String ROLE = VersionControl.class.toString();

    void login(String username, String password);
    void logout();
    
    /*
    void checkin(int projId);
    void checkout(int projId);
    void add(int projId);
    void remvoe(int projId);
    void diff(int projId_1, int projId_2);
    void log(int projId);
    void status(int projId);
    */
    
    void invoke(String command, String[] args);

}


