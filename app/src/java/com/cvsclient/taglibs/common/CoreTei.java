/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: CoreTei.java,v 1.2 2001/08/23 07:30:31 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  javax.servlet.jsp.tagext.*;

/**
 * TagExtraInfo for <b>core</b> tag, allows use of standard
 * &lt;jsp:getProperty/&gt; with the <b>core</b> tag script
 * variable id.
 *
 * @version $Revision: 1.2 $
 * @author  Hyoungsoo Yoon
 * @see     CoreTag
 */
public class CoreTei extends TagExtraInfo
{
    public final VariableInfo[] getVariableInfo(TagData data)
    {
        return new VariableInfo[]
        {
            new VariableInfo(
                data.getAttributeString("id"),
                Core.class.toString(),
                true,
                VariableInfo.AT_BEGIN
            ),
        };
    }
}

