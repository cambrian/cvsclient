/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: CoreTag.java,v 1.2 2001/08/23 07:30:31 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

/**
 * The CoreTag 
 *
 * @version $Revision: 1.2 $
 * @author  Hyoungsoo Yoon
 * @see     CoreTei
 */
public class CoreTag extends TagSupport
{
    private String m_message;
    
    public void setMessage(String message) {
        m_message = message;
    }


    //////////////////////////////////////////////////////////////////////
    // Tag interface
    //////////////////////////////////////////////////////////////////////

    /**
     * implementation of method from the Tag interface that tells the JSP what
     * to do upon encountering the start tag for this tag set
     *
     * @return SKIP_BODY - integer value telling the JSP engine to not evaluate
     *                     the body of this tag
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doStartTag() throws JspException  {
        if ( m_message != null ) {
            try {
                pageContext.getOut().print(m_message);
            } catch (Exception ex) {
                throw new JspException("IO Problem");
            }
            return Tag.SKIP_BODY;
        }
        return BodyTag.EVAL_BODY_BUFFERED;
    }

    
    //////////////////////////////////////////////////////////////////////
    // BodyTag interface
    //////////////////////////////////////////////////////////////////////

    /**
     *
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doAfterBody() throws JspException {
        if (m_message == null) {

        }
        return Tag.SKIP_BODY;
    }

}

