/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppTitleTag.java,v 1.1 2001/08/24 07:49:53 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

/**
 * The AppTitleTag 
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class AppTitleTag extends PhraseTag
{
    public AppTitleTag()
    {
        m_phrase = "CVSClient";
    }
}
