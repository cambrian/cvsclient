/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: IfDebugTag.java,v 1.3 2001/08/24 07:49:53 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

/**
 * The IfDebugTag 
 *
 * @version $Revision: 1.3 $
 * @author  Hyoungsoo Yoon
 * @see     Else
 */
public class IfDebugTag extends BodyTagSupport
{
    private final static int DEFAULT_LEVEL = 0;
    private int m_level=0;
    private int m_scope;
    
    public void setLevel(int level) {
        m_level = level;
    }
    
    public void setScope(String sc) throws JspException {
        if (sc.equalsIgnoreCase("session")) {
            m_scope = PageContext.SESSION_SCOPE;
        }
        else if (sc.equalsIgnoreCase("request")) { 
            m_scope = PageContext.REQUEST_SCOPE;
        }
        else if (sc.equalsIgnoreCase("application")) {
            m_scope = PageContext.APPLICATION_SCOPE;
        }
        else if (sc.equalsIgnoreCase("page")) {
            m_scope = PageContext.PAGE_SCOPE;
        }
        else {
            throw new JspException(
                "Scope must be page, request, session or application."
            );
        }
    }


    //////////////////////////////////////////////////////////////////////
    // Tag interface
    //////////////////////////////////////////////////////////////////////

    /**
     * implementation of method from the Tag interface that tells the JSP what
     * to do upon encountering the start tag for this tag set
     *
     * @return SKIP_BODY - integer value telling the JSP engine to not evaluate
     *                     the body of this tag
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doStartTag() throws JspException  {
        if (getLevel() <= DEFAULT_LEVEL) {
            return BodyTag.EVAL_BODY_BUFFERED;
        } else {
            // TODO: Process Else tag!!!
            return Tag.SKIP_BODY;
        }            
    }

    
    //////////////////////////////////////////////////////////////////////
    // BodyTag interface
    //////////////////////////////////////////////////////////////////////

    /**
     *
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doAfterBody() throws JspException {
        try {
            bodyContent.writeOut(bodyContent.getEnclosingWriter());
            return Tag.SKIP_BODY;
        } catch (Exception ex) {
            throw new JspException(ex.toString());
        }
    }

    //////////////////////////////////////////////////////////////////////
    // Implementation methods
    //////////////////////////////////////////////////////////////////////
    
    private int getLevel()
    {
        return m_level;
    }

}

