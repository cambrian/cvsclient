/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: Core.java,v 1.2 2001/08/23 07:30:31 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.JspException;

/**
 * The Core Taglib support class.
 *
 * @version $Revision: 1.2 $
 * @author  Hyoungsoo Yoon
 * @see     CoreTag
 */
public class Core
{

}

