/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PhraseTag.java,v 1.1 2001/08/24 07:49:53 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

/**
 * The PhraseTag 
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public abstract class PhraseTag extends TagSupport
{
    protected String m_phrase;

    public PhraseTag()
    {
        m_phrase = "";
    }


    //////////////////////////////////////////////////////////////////////
    // Tag interface
    //////////////////////////////////////////////////////////////////////

    /**
     * implementation of method from the Tag interface that tells the JSP what
     * to do upon encountering the start tag for this tag set
     *
     * @return SKIP_BODY - integer value telling the JSP engine to not evaluate
     *                     the body of this tag
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doStartTag() throws JspException  {
        if ( m_phrase != null ) {
            try {
                pageContext.getOut().print(m_phrase);
            } catch (Exception ex) {
                throw new JspException("IO Problem");
            }
        } else {
            //
        }
        return Tag.SKIP_BODY;
    }

}

