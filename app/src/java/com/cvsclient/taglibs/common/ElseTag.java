/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ElseTag.java,v 1.1 2001/08/24 07:49:53 hyoon Exp $
 */ 
package com.cvsclient.taglibs.common;

import  com.cvsclient.*;
import  java.util.*;
import  javax.servlet.jsp.*;
import  javax.servlet.jsp.tagext.*;

/**
 * The ElseTag 
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class ElseTag extends BodyTagSupport
{

    //////////////////////////////////////////////////////////////////////
    // Tag interface
    //////////////////////////////////////////////////////////////////////

    /**
     * implementation of method from the Tag interface that tells the JSP what
     * to do upon encountering the start tag for this tag set
     *
     * @throw JspException  thrown when error occurs in processing the body of
     *                       this method
     */
    public int doStartTag() throws JspException  {
        try {
            TagSupport parent = (TagSupport) findAncestorWithClass(this, TagSupport.class);
            if(parent == null) {
                //
            } else {
                //
            }
        } catch (Exception ex) {
            throw new JspException(ex.toString());
        }

        return BodyTag.EVAL_BODY_BUFFERED;
    }

}

