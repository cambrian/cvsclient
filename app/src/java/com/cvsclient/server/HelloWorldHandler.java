/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: HelloWorldHandler.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.server;

import  com.cvsclient.*;
import  com.cvsclient.service.HelloWorldService;
import  java.io.IOException;
import  java.io.InterruptedIOException;
import  java.io.PrintWriter;
import  java.net.Socket;
import  java.net.SocketException;
import  org.apache.avalon.framework.component.Component;
import  org.apache.avalon.framework.logger.AbstractLoggable;
import  org.apache.avalon.cornerstone.services.connection.ConnectionHandler;


/**
 * HelloWorldHandler
 * This handles an individual incoming request.  It outputs a greeting as html.
 *
 * @version $Revision: 1.1 $
 * @author Hyoungsoo Yoon
 */
public class HelloWorldHandler
    extends AbstractLoggable
    implements Component, ConnectionHandler
{
    protected static int        c_counter;

    protected String            m_greeting;

    protected HelloWorldHandler( final String greeting )
    {
        m_greeting = greeting;
    }

    /**
     * Handle a connection.
     * This handler is responsible for processing connections as they occur.
     *
     * @param socket the connection
     * @exception IOException if an error reading from socket occurs
     */
    public void handleConnection( final Socket socket )
        throws IOException
    {
        final String remoteHost = socket.getInetAddress().getHostName();
        final String remoteIP = socket.getInetAddress().getHostAddress();
        final PrintWriter out = new PrintWriter( socket.getOutputStream(), true );

        try
        {
            out.println( "<html><body><b>" + m_greeting + "!</b><br> Requests so far = " +
                         ++c_counter + "<br>" );
            out.println( "you are " + remoteHost + " at " + remoteIP + "<br>" );
            out.println( "</body></html>" );

            socket.close();
        }
        catch( final SocketException se )
        {
            getLogger().debug( "Socket to " + remoteHost + " closed remotely in HelloWorld", se );
        }
        catch( final InterruptedIOException iioe )
        {
            getLogger().debug( "Socket to " + remoteHost + " timeout in HelloWorld", iioe );
        }
        catch( final IOException ioe )
        {
            getLogger().debug( "Exception in HelloWorld handling socket to " + remoteHost ,
                               ioe );
        }
        catch( final Exception e )
        {
            getLogger().debug( "Exception in HelloWorld opening socket", e );
        }
        finally
        {
            try { socket.close(); }
            catch( final IOException ioe )
            {
                getLogger().error( "Exception closing socket ", ioe );
            }
        }

        getLogger().info( "Connection from " + remoteHost + " (" + remoteIP + ")" );
    }
}
