/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: CategoryType.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;


/**
 * CategoryType
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class CategoryType
{
    private final static String _CT_STR_MULTIMEDIA_APP = "Multimedia Application";

    public final static CategoryType CT_APP_MULTIMEDIA = new CategoryType(_CT_STR_MULTIMEDIA_APP);


    private String m_strType;
    
    
    public CategoryType() {
    }

    public CategoryType(String strType) {
        m_strType = strType;
    }


    public boolean equals(Object obj) {
        if ((obj != null) && (obj instanceof CategoryType)) {
            CategoryType ct = (CategoryType)obj;
            return (m_strType == ct.m_strType);
        } else {
            return false;
        }
    }

    public String toString() {
        return m_strType;
    }

    // Test routine
    public static void main(String[] args)
    {

    }

}

