/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: Module.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;

import  com.cvsclient.common.*;


/**
 * 
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public interface Module
{
    /**
     * Return the name for this <code>Module</code>.
     * <p>
     *
     * @return the Name for this <code>Module</code>
     */
    String          getName();

    String          getDescription();
    
    RepoInfo        getRepoInfo();

    Branch          getDefaultBranch();

    Branch[]        getBranches();

    CategoryType[]  getCategories();

    PlatformType[]  getPlatforms();
    
}

