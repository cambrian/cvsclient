/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PlatformType.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;


/**
 * PlatformType
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class PlatformType
{
    private final static String _PT_STR_WIN = "Windows";

    public final static PlatformType PT_WINDOWS = new PlatformType(_PT_STR_WIN);


    private String m_strType;
    
    
    public PlatformType() {
    }

    public PlatformType(String strType) {
        m_strType = strType;
    }


    public boolean equals(Object obj) {
        if ((obj != null) && (obj instanceof PlatformType)) {
            PlatformType pt = (PlatformType)obj;
            return (m_strType == pt.m_strType);
        } else {
            return false;
        }
    }

    public String toString() {
        return m_strType;
    }

    // Test routine
    public static void main(String[] args)
    {

    }

}

