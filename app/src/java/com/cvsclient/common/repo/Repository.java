/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: Repository.java,v 1.2 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;

import  com.cvsclient.common.*;


/**
 * 
 * @version $Revision: 1.2 $
 * @author  Hyoungsoo Yoon
 */
public interface Repository
{
    /**
     * Return the name for this <code>Repository</code>.
     * <p>
     *
     * @return the Name for this <code>Repository</code>
     */
    String      getName();

    RepoType    getType();

    String      getDescription();

    Module[]    getModules();
    
}

