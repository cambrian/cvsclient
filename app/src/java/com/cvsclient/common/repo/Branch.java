/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: Branch.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;

import  com.cvsclient.common.*;
import  java.io.File;


/**
 * 
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public interface Branch
{
    /**
     * Return the name for this <code>Branch</code>.
     * <p>
     *
     * @return the Name for this <code>Branch</code>
     */
    String      getName();

    String      getDescription();

    Version     getVersion();

    File        getRepositoryRoot();

    File        getWorkspaceRoot();
    
}

