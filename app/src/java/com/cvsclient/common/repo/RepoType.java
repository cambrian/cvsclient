/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoType.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;


/**
 * RepoType
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class RepoType
{
    private final static String _RT_STR_CVS = "Concurrent Versioning System";
    private final static String _RT_STR_VSS = "Visual SourceSafe";
    private final static String _RT_STR_RCC = "Rational ClearCase";

    public final static RepoType RT_CVS = new RepoType(_RT_STR_CVS);
    public final static RepoType RT_VSS = new RepoType(_RT_STR_VSS);
    public final static RepoType RT_RCC = new RepoType(_RT_STR_RCC);


    private String m_strType;
    
    
    public RepoType() {
    }

    public RepoType(String strType) {
        m_strType = strType;
    }


    public boolean equals(Object obj) {
        if ((obj != null) && (obj instanceof RepoType)) {
            RepoType rt = (RepoType)obj;
            return (m_strType == rt.m_strType);
        } else {
            return false;
        }
    }

    public String toString() {
        return m_strType;
    }

    // Test routine
    public static void main(String[] args)
    {

    }

}

