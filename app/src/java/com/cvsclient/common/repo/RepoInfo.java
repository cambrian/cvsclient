/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoInfo.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;

import  com.cvsclient.common.*;
import  java.util.Date;

/**
 * 
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public interface RepoInfo
{
    /**
     * Return the name for this <code>RepoInfo</code>.
     * <p>
     *
     * @return the Name for this <code>RepoInfo</code>
     */
    String      getName();

    Guid        getId();

    Repository  getRepository();

    AccessInfo  getAccessInfo();
    
    String      getNote();

    Date        getLastAccessTime();
    
}

