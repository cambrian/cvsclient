/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AccessInfo.java,v 1.1 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;


/**
 * AccessInfo
 *
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 */
public class AccessInfo
{
    private String username;
    private String password;
    private String hostname;
    private int    hostport;
    private String logonProtocol;

    
    public AccessInfo() {

    }

    public boolean equals(Object obj) {
        if ((obj != null) && (obj instanceof AccessInfo)) {
            AccessInfo info = (AccessInfo) obj;
            return (username.equals(info.username)); //... etc
        } else {
            return false;
        }
    }

    public String toString() {
        return null;
    }
    
    public static void main(String[] args)
    {

    }

}

