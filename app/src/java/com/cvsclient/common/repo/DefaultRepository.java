/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DefaultRepository.java,v 1.2 2001/09/02 23:12:43 hyoon Exp $
 */ 
package com.cvsclient.common.repo;


/**
 * <p> This class implements the <code>Repository</code> interface.
 * 
 * @version $Revision: 1.2 $
 * @author  Hyoungsoo Yoon
 * @see     Repository
 */
public class DefaultRepository implements Repository, java.io.Serializable
{
    /**
     * @serial
     */
    private String m_name;

    private String m_description;

    private RepoType m_repoType;

    private java.util.List m_modules;
    

    /**
     * Create a DefaultRepository with the given name.
     *
     * <p>
     *
     * @param name the name of the new <code>DefaultRepository</code>.
     *
     * @exception NullPointerException if the <code>name</code>
     *			is <code>null</code>.
     */
    public DefaultRepository(String name)
    {
        if (name == null)
            throw new NullPointerException("illegal null input");

        this.m_name = name;
    }

    /**
     * Return the name for this <code>DefaultRepository</code>.
     * <p>
     *
     * @return the Name for this <code>DefaultRepository</code>.
     */
    public String getName()
    {
        return m_name;
    }

    public RepoType    getType()
    {
        return m_repoType;
    }

    public String      getDescription()
    {
        return m_description;
    }

    public Module[]    getModules()
    {
        return (Module[]) m_modules.toArray();
    }



    /**
     * Return a string representation of this <code>DefaultRepository</code>.
     *
     * <p>
     *
     * @return a string representation of this <code>DefaultRepository</code>.
     */
    public String toString()
    {
        return("DefaultRepository:  " + m_name);
    }

    /**
     * Compares the specified Object with this <code>DefaultRepository</code>
     * for equality.  Returns true if the given object is also a
     * <code>DefaultRepository</code> and the two DefaultReposistories
     * have the same name.
     *
     * <p>
     *
     * @param o Object to be compared for equality with this
     *		<code>DefaultRepository</code>.
     *
     * @return true if the specified Object is equal equal to this
     *		<code>DefaultRepository</code>.
     */
    public boolean equals(Object o)
    {
        if (o == null)
            return false;

        if (this == o)
            return true;
 
        if (!(o instanceof DefaultRepository))
            return false;
        DefaultRepository that = (DefaultRepository)o;

        if (this.getName().equals(that.getName()))
            return true;
        return false;
    }
 
    /**
     * Return a hash code for this <code>DefaultRepository</code>.
     *
     * <p>
     *
     * @return a hash code for this <code>DefaultRepository</code>.
     */
    public int hashCode()
    {
        return m_name.hashCode();
    }
}

