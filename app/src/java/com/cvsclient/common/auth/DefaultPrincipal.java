/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DefaultPrincipal.java,v 1.1 2001/08/27 01:24:56 hyoon Exp $
 */ 
package com.cvsclient.common.auth;

import  java.security.Principal;


/**
 * <p> This class implements the <code>Principal</code> interface
 * and represents a default user.
 *
 * <p> Principals such as this <code>DefaultPrincipal</code>
 * may be associated with a particular <code>Subject</code>
 * to augment that <code>Subject</code> with an additional
 * identity.  Refer to the <code>Subject</code> class for more information
 * on how to achieve this.  Authorization decisions can then be based upon 
 * the Principals associated with a <code>Subject</code>.
 * 
 * @version $Revision: 1.1 $
 * @author  Hyoungsoo Yoon
 * @see java.security.Principal
 * @see javax.security.auth.Subject
 */
public class DefaultPrincipal implements Principal, java.io.Serializable
{
    /**
     * @serial
     */
    private String name;

    /**
     * Create a DefaultPrincipal with a Sample username.
     *
     * <p>
     *
     * @param name the Sample username for this user.
     *
     * @exception NullPointerException if the <code>name</code>
     *			is <code>null</code>.
     */
    public DefaultPrincipal(String name)
    {
        if (name == null)
            throw new NullPointerException("illegal null input");

        this.name = name;
    }

    /**
     * Return the Sample username for this <code>DefaultPrincipal</code>.
     *
     * <p>
     *
     * @return the Sample username for this <code>DefaultPrincipal</code>
     */
    public String getName()
    {
        return name;
    }

    /**
     * Return a string representation of this <code>DefaultPrincipal</code>.
     *
     * <p>
     *
     * @return a string representation of this <code>DefaultPrincipal</code>.
     */
    public String toString()
    {
        return("DefaultPrincipal:  " + name);
    }

    /**
     * Compares the specified Object with this <code>DefaultPrincipal</code>
     * for equality.  Returns true if the given object is also a
     * <code>DefaultPrincipal</code> and the two DefaultPrincipals
     * have the same username.
     *
     * <p>
     *
     * @param o Object to be compared for equality with this
     *		<code>DefaultPrincipal</code>.
     *
     * @return true if the specified Object is equal equal to this
     *		<code>DefaultPrincipal</code>.
     */
    public boolean equals(Object o)
    {
        if (o == null)
            return false;

        if (this == o)
            return true;
 
        if (!(o instanceof DefaultPrincipal))
            return false;
        DefaultPrincipal that = (DefaultPrincipal)o;

        if (this.getName().equals(that.getName()))
            return true;
        return false;
    }
 
    /**
     * Return a hash code for this <code>DefaultPrincipal</code>.
     *
     * <p>
     *
     * @return a hash code for this <code>DefaultPrincipal</code>.
     */
    public int hashCode()
    {
        return name.hashCode();
    }
}

