/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoDirectory.java,v 1.1 2001/08/30 04:22:49 hyoon Exp $
 */ 
package com.cvsclient;

/**
 * RepoDirectory
 *
 * @version $Revision: 1.1 $
 * @author Hyoungsoo Yoon
 */
public interface RepoDirectory
{
    String ROLE = RepoDirectory.class.toString();

    long[]   listRepositories();
    void     addRepository(long id);
    void     removeRepository(long id);

}


