/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ConcurrentVersioningSystem.java,v 1.2 2001/08/23 07:30:31 hyoon Exp $
 */ 
package com.cvsclient.protocol;


/**
 * ConcurrentVersioningSystem
 *
 * @version $Revision: 1.2 $
 * @author Hyoungsoo Yoon
 */
public interface ConcurrentVersioningSystem
{
    String ROLE = ConcurrentVersioningSystem.class.toString();

    void cvs_login(String username, String password);
    void cvs_logout();
    
    void cvs_checkin(int projId);
    void cvs_checkout(int projId);
    void cvs_add(int projId);
    void cvs_remvoe(int projId);
    void cvs_diff(int projId_1, int projId_2);
    void cvs_log(int projId);
    void cvs_status(int projId);

}




