<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Home | My Repository List</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/cvsclient.css" type="text/css">
</HEAD>
<BODY class="body">

<%-- Begin: Header --%>
<%@ include file="header.jspf" %>
<%-- End: Header --%>



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part  -->
    <TD vAlign="top" width="100%">

      <TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
            <TR class="header">
              <TD noWrap>My Repository List</TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>     
            <TR>
              <TD>

                <TABLE class="myrepo" cellPadding=3 cellSpacing=2 width="100%">
                  <THEAD>
                  <TR align=middle>
                    <TH noWrap width=-1><B>&nbsp;</B></TH>
                    <TH noWrap><B>Repository</B></TH>
                    <TH noWrap><B>Privilege</B></TH>
                    <TH noWrap><B>Date (Last Update)</B></TH>
                  </TR>
                  </THEAD>
                  <TBODY>
                  <TR class="odd-row">
                    <TD><A href="#"><IMG align=absBottom border=0 
                      src="<%=request.getContextPath()%>/images/folder.gif"></A></TD>
                    <TD><A href="<%=request.getContextPath()%>/templates/repo/repo.jsp" target="SUPERFRAME">My Repository 1</A></TD>
                    <TD align=middle>-</TD>
                    <TD>Thu Feb 10 20:31:39 2000</TD>
                  </TR>
                  <TR class="even-row">
                    <TD><A href="#"><IMG align=absBottom border=0 
                      src="<%=request.getContextPath()%>/images/folder.gif"></A></TD>
                    <TD><A href="<%=request.getContextPath()%>/templates/repo/repo.jsp" target="SUPERFRAME">My Repository 2</A></TD>
                    <TD align=middle>-</TD>
                    <TD>Thu Feb 10 20:31:39 2000</TD>
                  </TR>
                  </TBODY>
                </TABLE>

              </TD>
            </TR>
          </TABLE>
      
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part -->
    

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<%-- Begin: Footer --%>
<%@ include file="footer.jspf" %>
<%-- End: Footer --%>


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

