<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/log-1.0" prefix="log" %>
<%@ taglib uri="http://www.cvsclient.com/taglibs/common-1.0" prefix="cvsclient" %>

<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Home | Front Page</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/cvsclient.css" type="text/css">
</HEAD>
<BODY class="body">

<%-- Begin: Header --%>
<%@ include file="header.jspf" %>
<%-- End: Header --%>


<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part: Left Column -->
    <TD vAlign="top" width="60%">

      <TABLE border="0" cellPadding="5" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="fancybox">
            <TR class="header">
              <TD>What's New?</TD>
            </TR>
            <TR>
              <TD>

              ....

<BR/>
Session ID: <%= session.getId() %>
<BR/>


<log:debug>This is body content.</log:debug>
<log:debug message="This is attribute content." />
<log:debug category="test1">This is body content in category test1.</log:debug>

<cvsclient:core message="This is message." />

<cvsclient:ifdebug level="0">
level 0
</cvsclient:ifdebug>
<cvsclient:ifdebug level="<%= 0+1 %>">
level 1
</cvsclient:ifdebug>
<cvsclient:ifdebug level="3">
level 3
<%-- hahaha --%>
level 3
context path = <%=request.getContextPath()%>
</cvsclient:ifdebug>

...

Application Title: <cvsclient:apptitle/>

<BR/>
<jsp:useBean id="helloBean" class="com.cvsclient.beans.common.HelloBean" />
Original Message: <jsp:getProperty name="helloBean" property="message" />
<BR/>
<jsp:setProperty name="helloBean" property="message" value="Hello, New World!" />
New Message: <jsp:getProperty name="helloBean" property="message" />
<BR/>


              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD>My Repositories</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
            <TR>
              <TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>     
            <TR class="header">
              <TD>Account</TD>
            </TR>
            <TR>
              <TD>
                Sign-up<BR>
                Create a new repository
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD>New Repositories</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
            <TR>
              <TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>     
            <TR class="header">
              <TD>Popular Repositories</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
          </TABLE>
      
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part: Left Column -->
    
    <!-- Begin: Main Part: Right Column -->
    <TD vAlign="top" width="40%">

      <TABLE border="0" cellPadding="5" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="fancybox">
            <TR class="header">
              <TD noWrap>Services</TD>
            </TR>
            <TR>
              <TD>
                <DL>
                  <DT>CVS Web Client</DT>
                  <DD>...</DD>
                  <DT>Repository Hosting</DT>
                  <DD>...</DD>
                </DL>
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD noWrap>Help</TD>
            </TR>
            <TR>
              <TD>
                Getting Started<BR>
                Tutorial<BR>
                FAQs<BR>
                Documentation
              </TD>
            </TR>
            <TR>
              <TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>     
            <TR class="header">
              <TD noWrap>Information</TD>
            </TR>
            <TR>
              <TD>
                About CVS Client<BR>
                Terms and Conditions<BR>
                Contact Us
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        </TBODY>
      </TABLE>

    </TD>
    <!-- End: Main Part: Right Column -->

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<%-- Begin: Footer --%>
<%@ include file="footer.jspf" %>
<%-- End: Footer --%>




<h1>Test dump</h1>
<jsp:useBean id="p1" class="java.lang.String" scope="page" />
<H4>request</H4>
<log:dump scope="request" />
<H4>page</H4>
<log:dump scope="page" />
<H4>session</H4>
<log:dump scope="session" />
<H4>application</H4>
<log:dump scope="application" />



</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

