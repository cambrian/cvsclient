<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | My Account | Journals</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/cvsclient.css" type="text/css">
</HEAD>
<BODY class="body">

<%-- Begin: Header --%>
<%@ include file="header.jspf" %>
<%-- End: Header --%>



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part  -->
    <TD vAlign="top" width="100%">

      <TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
            <TR class="header">
              <TD noWrap>Journals</TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>     
            <TR>
              <TD>

              ....

              </TD>
            </TR>
          </TABLE>
      
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part -->
    

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<%-- Begin: Footer --%>
<%@ include file="footer.jspf" %>
<%-- End: Footer --%>


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

