<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Login Window</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/cvsclient.css" type="text/css">
<SCRIPT type="text/javascript" language="JavaScript">
function closeSelf() {
    self.close();
}
</SCRIPT>
</HEAD>


<BODY class="body">

<%-- Begin: Header --%>
<%@ include file="header.jspf" %>
<%-- End: Header --%>



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part  -->
    <TD vAlign="top" width="100%">

      <TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
            <TR class="header">
              <TD noWrap>Login to CVSClient.com</TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>     
            <TR>
              <TD align="center">

                <TABLE border=0 cellPadding=0 cellSpacing=0 width="98%">
                  <TBODY>
                    <TR>
                      <TD width="78%" vAlign="top">

<%-- Begin: Login Form --%>
<%@ include file="loginform.jspf" %>
<%-- End: Login Form --%>

                      </TD>
                      <TD width="2%" vAlign="top">
                         &nbsp;
                      </TD>
                      <TD width="18%" vAlign="top">

                      <A HREF="javascript:closeSelf();" NAME="OK">OK</A>
                      <BR/>
                      <A HREF="javascript:closeSelf();" NAME="CANCEL">Cancel</A>

                      </TD>
                    </TR>
                  </TBODY>
                </TABLE>

              
              </TD>
            </TR>
          </TABLE>
      
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part -->
    

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<%-- Begin: Footer --%>
<%@ include file="footer.jspf" %>
<%-- End: Footer --%>


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->

