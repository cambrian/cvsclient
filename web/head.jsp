<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client</TITLE>
<LINK rel="STYLESHEET" href="/styles/cvsclient.css" type="text/css">
<SCRIPT type="text/javascript" language="JavaScript">
function focusOnUsername() {
    document.loginform.username.focus();
}
</SCRIPT>
</HEAD>


<BODY class="head" onLoad="focusOnUsername()" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">

<TABLE margin="0" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>
    <TD rowSpan="2" height="70" width="60%" vAlign="top">
      <IMG src="/images/logo.gif" alt="CVS Client Logo" height="70" width="280">
    </TD>
    <TD align="right" height="20" vAlign="top"><IMG alt="" border=0 height=20 
      src="/images/curve.gif" width=18></TD>
    <TD class="menu" align="right" height="20" vAlign="center" noWrap >&nbsp;&nbsp;&nbsp;<A
      href="/superframeset.jsp" target="SUPERFRAME">Home</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="/repo/repo.jsp" target="SUPERFRAME">Repository</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="/repo/workbench/workbench.jsp" target="SUPERFRAME">Workbench</A>&nbsp;&nbsp;||
      &nbsp;&nbsp;<A href="/docs/" target="SUPERFRAME">Help</A>&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
    <TD height="50" vAlign="top" width=19><IMG alt="" border=0 height="50"
      src="/images/blank.gif" width=19></TD>
    <TD bgColor="white" align=right height="50" vAlign="top" noWrap>

      <TABLE height="50" width="240" margin="0" border="0" cellPadding="0" cellSpacing="0">
        <FORM name="loginform" method="post" action="">
          <TBODY>
          <TR bgColor="white" vAlign="top" height="80%">
            <TD><IMG alt="" border=0 height="1" width="1" src="/images/blank.gif"></TD>
          </TR>
          <TR vAlign="bottom">
          <TD>
            <TABLE width="100%" height="100%" margin="0" border="1" cellPadding="5" cellSpacing="0">          
              <TR class="loginbox">
                <TD vAlign="bottom">
                Username:&nbsp;
                <INPUT name="username" type="text" value="" size="10">&nbsp;
                <INPUT type="submit" value="Login">
                </TD>
              </TR>
            </TABLE>
          </TD>
          </TR>
          </TBODY>
        </FORM>
      </TABLE>

    </TD>
  </TR>
  </TBODY>
</TABLE>

</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->


