<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository Directory | Menu</TITLE>
<LINK rel="STYLESHEET" href="/styles/sidemenu.css" type="text/css">
<SCRIPT type="text/javascript" language="JavaScript1.2" src="/scripts/resize.js"></SCRIPT>
<SCRIPT type="text/javascript" language="JavaScript1.2" src="/scripts/collapsiblelist.js"></SCRIPT>
<SCRIPT type="text/javascript" language="JavaScript">
var menus = new Array();

menus[0] = new Object();
menus[0].title = "<A href='directory.jsp' target='BODY' onMouseOver=\"self.status='Repository Directory'; return true;\" onMouseOut=\"self.status=''; return true;\">Repository List</A>";
menus[0].submenus = new Array();
menus[0].submenus[0] = new Object();
menus[0].submenus[0].title = "<A href='alphabetical.jsp' target='BODY'>Alphabetical Listing</A>";
menus[0].submenus[1] = new Object();
menus[0].submenus[1].title = "<A href='favorites.jsp' target='BODY'>Favorites</A>";
menus[0].submenus[2] = new Object();
menus[0].submenus[2].title = "<A href='search.jsp' target='BODY'>Search</A>";
menus[0].submenus[3] = new Object();
menus[0].submenus[3].title = "<A href='submit.jsp' target='BODY'>Submit</A>";

menus[1] = new Object();
menus[1].title = "<A href='bycategory.jsp' target='BODY' onMouseOver=\"self.status='Repository Listing by Categories'; return true;\" onMouseOut=\"self.status=''; return true;\">List by Categories</A>";
menus[1].submenus = new Array();
menus[1].submenus[0] = new Object();
menus[1].submenus[0].title = "<A href='listbycategory.jsp' target='BODY'>Software</A>";
menus[1].submenus[1] = new Object();
menus[1].submenus[1].title = "<A href='listbycategory.jsp' target='BODY'>Web Applications</A>";

menus[2] = new Object();
menus[2].title = "<A href='byplatform.jsp' target='BODY' onMouseOver=\"self.status='Repository Listing by Categories'; return true;\" onMouseOut=\"self.status=''; return true;\">List by Platforms</A>";
menus[2].submenus = new Array();
menus[2].submenus[0] = new Object();
menus[2].submenus[0].title = "<A href='listbyplatform.jsp' target='BODY'>Windows</A>";
menus[2].submenus[1] = new Object();
menus[2].submenus[1].title = "<A href='listbyplatform.jsp' target='BODY'>Unix/Linux</A>";
menus[2].submenus[2] = new Object();
menus[2].submenus[2].title = "<A href='listbyplatform.jsp' target='BODY'>Java</A>";


var width=200;
var height = 22;
var indent = 15;
var xoffset = 5;
var yoffset = 20;
var firstBgColor = "#E8EEBF";
var secondBgColor = "#F9FFCF";

function colorize(id) {
  if(this.visible == true) {
    if(this.bgColor != secondBgColor) {
      this.bgColor = secondBgColor;
      this.needsUpdate = true;
    }
    for(var i = 0; i < document.lists.length; i++) {
      document.lists[i].touch = true;
      if(i != id) {
	if(document.lists[i].bgColor != firstBgColor) {
	  document.lists[i].bgColor = firstBgColor;
	  document.lists[i].needsUpdate = true;
	}
      }
    }
  }
  var p = this;
  while(p != null) { p.touch = false; p = p.parent; }
  for(var i = 0; i < document.lists.length; i++)
    if(document.lists[i].touch &&
       document.lists[i].visible)
      expand(document.lists[i].id);
}

function init() {
  if(parseInt(navigator.appVersion) < 4) {
    document.write("<TABLE width="+width+" border=0 cellspacing=0 cellpadding=0");
    document.write("<TR><TD width="+xoffset+" height="+yoffset+">&nbsp;</TD><TD>&nbsp;</TD></TR>");
    document.write("<TR><TD width="+xoffset+">&nbsp;</TD><TD>");
    document.write("<TABLE width=100% bgColor="+firstBgColor+" border=0 cellspacing=0 cellpadding=0");
    for(var i=0;i<menus.length;i++) {
      if(menus[i].submenus == null || menus[i].submenus.length == 0) {
        document.write("<TR height="+height+"><TD colspan=2><IMG border=0 src='/images/expanded.gif' alt='-'>&nbsp;<SPAN class='firstlevel'>"+menus[i].title+"</SPAN></TD></TR>");        
      } else {
        document.write("<TR height="+height+"><TD colspan=2><IMG border=0 src='/images/expanded.gif' alt='-'>&nbsp;<SPAN class='firstlevel'>"+menus[i].title+"</SPAN></TD></TR>");
        for(var j=0;j<menus[i].submenus.length;j++) {
          document.write("<TR height="+height+"><TD width="+indent+">&nbsp;</TD><TD align=left width=90%><SPAN class='secondlevel'>"+menus[i].submenus[j].title+"</SPAN></TD></TR>");
        }
      }
    }
    document.write("</TABLE>");
    document.write("</TD></TR>");
    document.write("</TABLE>");
  } else {
    var l = new List(true, width, height, firstBgColor);
    l.setFont("<SPAN class='firstlevel'>","</SPAN>");
    for(var i=0;i<menus.length;i++) {
      if(menus[i].submenus == null || menus[i].submenus.length == 0) {
        l.addItem(menus[i].title);        
      } else {
        var m = new List(false, width, height, secondBgColor);
        m.setIndent(indent);
        m.setFont("<SPAN class='secondlevel'>","</SPAN>");
        m.onexpand = colorize;
        for(var j=0;j<menus[i].submenus.length;j++) {
          m.addItem(menus[i].submenus[j].title);
        }
        l.addList(m, menus[i].title);
      }
    }
    l.build(xoffset,yoffset);
  }
  return;
}
</SCRIPT>
</HEAD>
<BODY onLoad="init();" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">

<DIV ID="spacer"></DIV>
<DIV ID="lItem0" NAME="lItem0"></DIV>
<DIV ID="lItem1" NAME="lItem1"></DIV>
<DIV ID="lItem2" NAME="lItem2"></DIV>
<DIV ID="lItem3" NAME="lItem3"></DIV>
<DIV ID="lItem4" NAME="lItem4"></DIV>
<DIV ID="lItem5" NAME="lItem5"></DIV>
<DIV ID="lItem6" NAME="lItem6"></DIV>
<DIV ID="lItem7" NAME="lItem7"></DIV>
<DIV ID="lItem8" NAME="lItem8"></DIV>
<DIV ID="lItem9" NAME="lItem9"></DIV>
<DIV ID="lItem10" NAME="lItem10"></DIV>
<DIV ID="lItem11" NAME="lItem11"></DIV>
<DIV ID="lItem12" NAME="lItem12"></DIV>
<DIV ID="lItem13" NAME="lItem13"></DIV>
<DIV ID="lItem14" NAME="lItem14"></DIV>
<DIV ID="lItem15" NAME="lItem15"></DIV>
<DIV ID="lItem16" NAME="lItem16"></DIV>
<DIV ID="lItem17" NAME="lItem17"></DIV>
<DIV ID="lItem18" NAME="lItem18"></DIV>
<DIV ID="lItem19" NAME="lItem19"></DIV>
<DIV ID="lItem20" NAME="lItem20"></DIV>
<DIV ID="lItem21" NAME="lItem21"></DIV>
<DIV ID="lItem22" NAME="lItem22"></DIV>
<DIV ID="lItem23" NAME="lItem23"></DIV>
<DIV ID="lItem24" NAME="lItem24"></DIV>
<DIV ID="lItem25" NAME="lItem25"></DIV>
<DIV ID="lItem26" NAME="lItem26"></DIV>
<DIV ID="lItem27" NAME="lItem27"></DIV>
<DIV ID="lItem28" NAME="lItem28"></DIV>
<DIV ID="lItem29" NAME="lItem29"></DIV>
<DIV ID="lItem30" NAME="lItem30"></DIV>
<DIV ID="lItem31" NAME="lItem31"></DIV>
<DIV ID="lItem32" NAME="lItem32"></DIV>
<DIV ID="lItem33" NAME="lItem33"></DIV>
<DIV ID="lItem34" NAME="lItem34"></DIV>
<DIV ID="lItem35" NAME="lItem35"></DIV>
<DIV ID="lItem36" NAME="lItem36"></DIV>
<DIV ID="lItem37" NAME="lItem37"></DIV>
<DIV ID="lItem38" NAME="lItem38"></DIV>
<DIV ID="lItem39" NAME="lItem39"></DIV>
<DIV ID="lItem40" NAME="lItem40"></DIV>
<DIV ID="lItem41" NAME="lItem41"></DIV>
<DIV ID="lItem42" NAME="lItem42"></DIV>
<DIV ID="lItem43" NAME="lItem43"></DIV>
<DIV ID="lItem44" NAME="lItem44"></DIV>
<DIV ID="lItem45" NAME="lItem45"></DIV>
<DIV ID="lItem46" NAME="lItem46"></DIV>
<DIV ID="lItem47" NAME="lItem47"></DIV>
<DIV ID="lItem48" NAME="lItem48"></DIV>
<DIV ID="lItem49" NAME="lItem49"></DIV>
<DIV ID="lItem50" NAME="lItem50"></DIV>


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->

