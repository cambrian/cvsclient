<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository | Work Bench</TITLE>
<LINK rel="STYLESHEET" href="/styles/workbench.css" type="text/css">
</HEAD>

<FRAMESET rows="*,140" frameborder="0" border="0" framespacing="0" framepadding="0">
    <FRAMESET cols="200,*" frameborder="0" border="0" framespacing="0" framepadding="0">
        <FRAMESET rows="20,*" frameborder="0" border="0" framespacing="0" framepadding="0">
            <FRAME src="sidetitle.jsp" name="SIDETITLE" 
                   marginheight="0" marginwidth="0"
                   scrolling="no" noresize>
            <FRAME src="project.jsp" name="SIDEMENU" 
                   marginheight="0" marginwidth="0"
                   scrolling="yes">
        </FRAMESET>
        <FRAMESET rows="20,*" frameborder="0" border="0" framespacing="0" framepadding="0">
            <FRAME src="bodytitle.jsp" name="BODYTITLE" 
                   marginheight="0" marginwidth="0"
                   scrolling="no" noresize>
            <FRAME src="workspace.jsp" name="BODY"
                   marginheight="0" marginwidth="0"
                   scrolling="yes">
        </FRAMESET>
    </FRAMESET>
    <FRAMESET rows="20,*" frameborder="0" border="0" framespacing="0" framepadding="0">
        <FRAME src="foottitle.jsp" name="FOOTTITLE" 
               marginheight="0" marginwidth="0"
               scrolling="no" noresize>
        <FRAME src="log.jsp" name="FOOT" 
               marginheight="0" marginwidth="0"
               scrolling="yes">
    </FRAMESET>
</FRAMESET>

</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->


