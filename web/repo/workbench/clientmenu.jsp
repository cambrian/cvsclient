<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client</TITLE>
<LINK rel="STYLESHEET" href="/styles/workbench.css" type="text/css">
</HEAD>


<BODY class="topmenu" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">

<TABLE margin="0" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>
    <TD height="20" vAlign="center" noWrap>
      &nbsp;&nbsp;<A href="manager.jsp"
      onMouseOver="self.status='Repository Manager'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Repo&nbsp;Manager</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="studio.jsp"
      onMouseOver="self.status='Project Studio'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Project&nbsp;Studio</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="builder.jsp"
      onMouseOver="self.status='Source Builder'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Source&nbsp;Builder</A>&nbsp;&nbsp;|
      &nbsp;&nbsp;<A href="tools.jsp"
      onMouseOver="self.status='Tools'; return true;"
      onMouseOut="self.status=''; return true;"
      target="BODYFRAME">Tools</A>&nbsp;&nbsp;|
    </TD>
  </TR>
  </TBODY>
</TABLE>
      
</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
-->


