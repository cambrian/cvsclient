<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>CVS Client | Repository | New Bugs</TITLE>
<LINK rel="STYLESHEET" href="/styles/repo.css" type="text/css">
</HEAD>
<BODY class="body">

<!-- Begin: Header -->
<DIV class="date">
<SCRIPT type="text/javascript" language="JavaScript" src="/scripts/currenttime.js"></SCRIPT>
</DIV>
<DIV align="center">
<TABLE class="footer" border=0 cellPadding=1 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD class="horzbar" width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="2"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="10"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
<!-- End: Header -->



<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part -->
    <TD vAlign="top" width="100%">

      <TABLE border="0" cellPadding="5" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>
      
          <TABLE class="fancybox">
            <TR class="header">
              <TD>New Bugs</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        <TR><TD>
                  
          <TABLE class="fancybox">
            <TR class="header">
              <TD>Submit</TD>
            </TR>
            <TR>
              <TD>
              ...
              </TD>
            </TR>
          </TABLE>
                
        </TD></TR>
        </TBODY>
      </TABLE>
      
    </TD>
    <!-- End: Main Part -->

  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


<!-- Begin: Footer -->
<DIV align="center">
<TABLE class="footer" border=0 cellPadding=1 cellSpacing=0 width="100%">
  <TBODY>
  <TR>
    <TD width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="10"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD class="horzbar" width="*">
      <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
        <TBODY>
        <TR><TD height="2"></TD></TR>
        </TBODY>
      </TABLE>
    </TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
<DIV class="copyright">
<SCRIPT type="text/javascript" language="JavaScript" src="/scripts/footer.js"></SCRIPT>
</DIV>
<!-- End: Footer -->


</BODY>
</HTML>

<!--
  Copyright (c) 2000 CVSClient.Com.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
-->


